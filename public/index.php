<?php

header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    setcookie('exit', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    $messages['save'] = 'Спасибо, результаты сохранены.';
  if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['data'] = !empty($_COOKIE['data_error']);
  $errors['Limbs'] = !empty($_COOKIE['Limbs_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['Biografia'] = !empty($_COOKIE['Biografia_error']);
  $errors['Контракт'] = !empty($_COOKIE['Контракт_error']);
  
  if ($errors['fio']) {
    setcookie('fio_error', '', 100000);
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if($errors['email']) {
  setcookie('email_error', '', 100000);
  $messages[] = '<div class="error">Заполните почту.</div>';
  }
  if($errors['data']) {
  setcookie('data_error', '', 100000);
  $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if($errors['Limbs']) {
  setcookie('Limbs_error', '', 100000);
  $messages[] = '<div class="error">Выберете количество.</div>';
  }
  if($errors['abilities']) {
  setcookie('abilities_error', '', 100000);
  $messages[] = '<div class="error">Выберете способность.</div>';
  }
  if($errors['Biografia']) {
  setcookie('Biografia_error', '', 100000);
  $messages[] = '<div class="error">Заполните Биографию.</div>';
  }
  
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['data'] = empty($_COOKIE['data_value']) ? '' : $_COOKIE['data_value'];
  $values['Limbs'] = empty($_COOKIE['Limbs_value']) ? '' : $_COOKIE['Limbs_value'];
  $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];
  $values['Biografia'] = empty($_COOKIE['Biografia_value']) ? '' : $_COOKIE['Biografia_value'];
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
      
  $user = 'db12135454';
  $pass = '12345465';
  $db = new PDO('mysql:host=localhost;dbname=test', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  
   try {

        $stmt = $db->prepare("SELECT name FROM application1 WHERE email = ?");
        $stmt -> execute([$_SESSION['login']]);
        $values['fio'] = $stmt->fetchColumn();
        
        $values['email'] = $_SESSION['login'];
        
        $stmt = $db->prepare("SELECT year FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['date'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT pol FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['pol'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT Limbs FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['Limbs'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT abilities FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['abilities'] = $stmt->fetchColumn();
        
        $stmt = $db->prepare("SELECT Biografia FROM application1 WHERE email = ?");
        $stmt->execute([$_SESSION['login']]);
        $values['Biografia'] = $stmt->fetchColumn();
        

        
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    } 

    $messages[] ='Вход с логином ';
    $messages[] = $_SESSION['login'];
  }
  
  include('form.php');
  }
  
  else {
  $errors = FALSE;
  if (!preg_match("#^[А-Яа-яЁё]+\s[А-Яа-яЁё]+\s[А-Яа-яЁё]+$#i", $_POST['fio'])) {
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }
  
  if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  setcookie('email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['date'])) {
  setcookie('date_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['pol'])) {
  setcookie('pol_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('pol_value', $_POST['pol'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['Limbs'])) {
  setcookie('Limbs_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('Limbs_value', $_POST['Limbs'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['Biografia'])) {
  setcookie('Biografia_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('Biografia_value', $_POST['Biografia'], time() + 30 * 24 * 60 * 60);
}

if (empty($_POST['abilities'])) {
  setcookie('abilities_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('abilities_value', $_POST['abilities'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['Контракт'])) {
  setcookie('Контракт_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('Контракт_value', $_POST['Контракт'], time() + 30 * 24 * 60 * 60);
}
  
if ($errors) {
  header('Location: index.php');
  exit();
  }
else {
  setcookie('fio_error', '', 100000);
  setcookie('email_error', '', 100000);
  setcookie('date_error', '', 100000);
  setcookie('pol_error', '', 100000);
  setcookie('Limbs_error', '', 100000);
  setcookie('Biografia_error', '', 100000);
  setcookie('Контракт_error', '', 100000);
  setcookie('abilities_error', '', 100000);
} 
  switch($_POST['pol']) {
    case 'm': {
        $pol='m';
        break;
    }
    case 'f':{
        $pol='f';
        break;
    }
};

  switch($_POST['Limbs']) {
    case '1': {
        $limbs='1';
        break;
    }
    case '2':{
        $limbs='2';
        break;
    }
    case '3':{
        $limbs='3';
        break;
    }
    case '4':{
        $limbs='4';
        break;
    }
};
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
  $user = 'db12135454';
  $pass = '12345465';
  $db = new PDO('mysql:host=localhost;dbname=db12135454', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $messages[] = "email изменен не будет";
  try {
        $stmt = $db->prepare("UPDATE application1 SET name = ?, year = ?, pol = ?, limbs = ?, abilities = ?, biography = ? WHERE email = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['date'],$pol,$limbs,$abilities,$_POST['Biografia'],$_SESSION['login']));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    setcookie('save', '1');
    }
    else {
      $user = 'db12135454';
      $pass = '12345465';
      $db = new PDO('mysql:host=localhost;dbname=db12135454', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
      try {
      $stmt = $db->prepare("SELECT email FROM application1 WHERE email = ?");
      $stmt -> execute([$_POST['email']]);
      $log = $stmt->fetchColumn();
      }
      catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
      }
      if ($log != NULL){
          setcookie('exit', '1');
      }
      else {
    $login = $_POST['email'];
    $password = substr(rand(10000,100000), 2);
    setcookie('login', $login);
    setcookie('pass', $password);
    $password1 = md5($password);
     try {
        $stmt = $db->prepare("INSERT INTO application1 SET name = ?, email = ?, year = ?, pol = ?, limbs = ?, abilities = ?, biography = ?, pass = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['email'],$_POST['date'],$pol,$limbs,$abilities,$_POST['Biografia'], $password1));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    setcookie('save', '1');
    }
  }
  $new_url = 'index.php';
  header('Location: '.$new_url);
}
?>    
