<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title> Задание 3 (Главная страница) </title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <link rel="stylesheet" href="style.css"/>
</head>
 <body>
 
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}
?>
 
 <div class="nav">
    <form id="start" action="" method="post">
		<label> Введите имя 
            <br>
            <input name=fio type = "text" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"> </label>
		<br>
		<br>
		<label> Введите почту 
			<br>
			<input name="email" type = "email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"> </label>
		<br>
		<br>
		<label> Введите дату рождения 
			<br>
			<input name="data" type = "date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>"> </label>
		<br>
		<br>
		<div> Выберите пол
			<br> 
			<label <?php if ($errors['pol']) {print 'class="error1"';} ?>>
				<input type = "radio" value = "m" name = "pol" <?php if ($values['pol']=="m") {print 'checked';} ?>> муж </label>
			<label <?php if ($errors['pol']) {print 'class="error1"';} ?>>
				<input type = "radio" value = "f" name = "pol" <?php if ($values['pol']=="f") {print 'checked';} ?>> жен </label>
		</div>
		<br>
		<div> Количество конечностей
			<br>
			<label <?php if ($errors['Limbs']) {print 'class="error1"';} ?>>
				<input type = "radio" value = "1" name = "Limbs" <?php if ($errors['Limbs']) {print 'class="error"';} ?> <?php if ($values['Limbs']=="1") {print 'checked';} ?>> 1
			</label>
			<label <?php if ($errors['Limbs']) {print 'class="error1"';} ?>>
				<input type = "radio" value = "2" name = "Limbs"  <?php if ($values['Limbs']=="2") {print 'checked';} ?> > 2
			</label>
			<label <?php if ($errors['Limbs']) {print 'class="error1"';} ?>>
				<input type = "radio" value = "3" name = "Limbs" <?php if ($values['Limbs']=="3") {print 'checked';} ?> > 3
			</label>
			<label <?php if ($errors['Limbs']) {print 'class="error1"';} ?>>
				<input type = "radio" value = "4" name = "Limbs" <?php if ($values['Limbs']=="4") {print 'checked';} ?> > 4
			</label>
		</div>
		<br>
		<div> Сверхспособности:
			<br>
			<select name="abilities[]" multiple = "multiple" required>
				<option value = "Значение 1" <?php if (strripos($values['abilities'], "Значение 1")) {print 'selected';} ?>> Бессмертие </option>
				<option value = "Значение 2" <?php if (strripos($values['abilities'], "Значение 2")) {print 'selected';} ?>> Прохождение сквозь стены </option>
				<option value = "Значение 3" <?php if (strripos($values['abilities'], "Значение 3")) {print 'selected';} ?>> Левитация </option>
			</select>
			<br>
		</div>
		<br>
		Биография
		<br>
		<textarea name = "Biografia" cols = "40" rows = "10" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php print $values['biography']; ?>> </textarea>
		<br>
		<br>
		<label <?php if ($errors['check']) {print 'class="error1"';} ?>>
			<input type = "checkbox" name = "Контракт" <?php if ($errors['check']) {print 'class="error"';} ?>> С контрактом ознакомлен
		</label>
		<br>
		<br>
		<input type = "submit" name="done" value = "Отправить"/>
	</form>
	</div>
</body>
</html> 
